import { createApolloFetch } from 'apollo-fetch';

const apolloFetch = createApolloFetch({
  uri: `${process.env.REACT_APP_API_GRAPHQL_HOST}/graphql`,
});

export const setAuthorizationHeader = (token) => {
  apolloFetch.use(({ _, options }, next) => {
    if (!options.headers) {
      options.headers = {};
    }
    options.headers.Authorization = `Bearer ${token}`;
    next();
  });
};

export default apolloFetch;
