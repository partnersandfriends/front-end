
import React, { useState } from 'react';

const Card = ({ expense }) => {
  const [showDetails, setShowDetails] = useState(false);

  return (
    <div className="card box-shadow">
      {
        expense && (
          <>
            <h4>{expense.companyName}</h4>

            <div>
              <span>
                R$
                {expense.value}
              </span>
            </div>

            <br />
            {
              expense.details && (<div><button type="button" onClick={() => setShowDetails(!showDetails)}>Ver detalhes</button></div>)
            }
            {
              showDetails && expense.details ? (
                <div className="card-detail">
                  <p>
                    <b>CNPJ: </b>
                    {expense.details.cnpj}
                  </p>
                  <p>
                    <img alt="mapa da localização da transação" src={expense.details.mapLocation} />
                  </p>
                </div>
              ) : ''
            }
          </>
        )
      }
    </div>
  );
};


export default Card;
