import React, { useState } from 'react';
import { MUTATION_SIMULATE } from '../graphql';
import apolloFetch from '../utils/apolloFetch';

const customParseFloat = (value) => (value ? parseFloat(value) : 0);

const validateRequest = (interestRate, loanAmount, days) => ({
  interestRate: customParseFloat(interestRate),
  loanAmount: customParseFloat(loanAmount),
  days: customParseFloat(days),
});

const Simulation = () => {
  const [interestRate, setInterestRate] = useState();
  const [loanAmount, setLoanAmount] = useState();
  const [days, setDays] = useState();
  const [simulate, setSimulate] = useState({});


  const submit = () => {
    const simulation = validateRequest(interestRate, loanAmount, days);

    apolloFetch({
      query: MUTATION_SIMULATE,
      variables: { simulation },
    }).then(({ data }) => {
      setSimulate(data.simulate);
    }).catch((error) => {
      console.log(error);
    });
  };

  return (
    <form className="form">
      <br />
      <h3>Simulação de Empréstimo</h3>
      <div>
        <label htmlFor="interestRate">
          Taxa de juro
          <input type="number" name="interestRate" required onKeyUp={(e) => setInterestRate(e.target.value)} />
        </label>

        <label htmlFor="loanAmount">
          Valor do empréstimo
          <input type="number" name="loanAmount" required onKeyUp={(e) => setLoanAmount(e.target.value)} />
        </label>

        <label htmlFor="days">
          Dias de uso
          <input type="number" name="days" required onKeyUp={(e) => setDays(e.target.value)} />
        </label>
      </div>
      <div>
        <button type="button" onClick={submit}>Calcular</button>
      </div>
      <br />
      <div>
        {simulate
          && (
            <>
              <span>{ simulate.value}</span>
              {
              simulate.tax && (
              <span style={{ fontSize: 14 }}>
                <span>
                  {' +IOF '}
                </span>
                { simulate.tax}
              </span>
              )
           }
            </>
          )}
      </div>
    </form>
  );
};

export default Simulation;
