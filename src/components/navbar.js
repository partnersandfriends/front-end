import React from 'react';
import { Link } from 'react-router-dom';
import { useAuth0 } from '../auth/react-auth0-spa';

const NavBar = () => {
  const { isAuthenticated, loginWithRedirect, logoutWithRedirect } = useAuth0();

  return (
    <nav>
      {!isAuthenticated && (
        <button onClick={() => loginWithRedirect({})} type="button">
              Log in
        </button>
      )}
      {isAuthenticated && (
        <span>
          <Link to="/">Dashboard</Link>
          <Link to="/profile">Perfil</Link>
          <button onClick={() => logoutWithRedirect({})} type="button">
              Log out
          </button>
        </span>
      )}
    </nav>
  );
};

export default NavBar;
