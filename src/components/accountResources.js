import React from 'react';

const AccountResources = ({ accountResources }) => (
  <div>
    { accountResources && (
      <>
        <p>
          <b>Cheque especial </b>
          <br />
      R$
          {accountResources.overdraft}
        </p>
        <p>
          <b>Credito</b>
          <br />
      R$
          {accountResources.credit}
        </p>
        <p>
          <b>Investimentos</b>
          <br />
      R$
          {accountResources.investments}
        </p>
      </>
    )}
    {
  !accountResources && (
    <p className="error">Não foi possível carregar os dados</p>
  )
}
  </div>
);

export default AccountResources;
