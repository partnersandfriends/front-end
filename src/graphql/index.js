export const QUERY_EXPENSES = `
query($idUser: ID!) {
    expenses(idUser: $idUser) {
        companyName
        idUser
        value
        details{
          cnpj
          mapLocation
        }
    }
    accountResources{
      overdraft
      credit
      investments
    }
}`;

export const MUTATION_SIMULATE = `
mutation($simulation: SimulationInput!) {
  simulate(simulation: $simulation) {
    value
    tax
  }
}`;
