import React from 'react';
import './App.css';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import NavBar from './components/navbar';
import Home from './pages/home';
import Profile from './pages/profile';
import Dashboard from './pages/dashboard';
import AuthenticatedRoute from './components/authenticatedRoute';

const App = () => (
  <div>
    <BrowserRouter>
      <header>
        <NavBar />
      </header>
      <Switch>
        <Route path="/" exact component={Dashboard} />
        <AuthenticatedRoute path="/profile" component={Profile} />
        <AuthenticatedRoute path="/home" component={Home} />
      </Switch>
    </BrowserRouter>
  </div>
);

export default App;
