import React, { useEffect, useState } from 'react';
import Card from '../components/card';
import { QUERY_EXPENSES } from '../graphql';
import Simulation from '../components/simulation';
import AccountResources from '../components/accountResources';
import apolloFetch, { setAuthorizationHeader } from '../utils/apolloFetch';
import { useAuth0 } from '../auth/react-auth0-spa';

const Home = () => {
  const [expenses, setExpenses] = useState([]);
  const [accountResources, setAccountResources] = useState({});
  const { getTokenSilently, isAuthenticated } = useAuth0();
  const fetchData = async () => {
    if (isAuthenticated) {
      const token = await getTokenSilently();
      setAuthorizationHeader(token);
    }
    apolloFetch({
      query: QUERY_EXPENSES,
      variables: { idUser: '5000' },
    }).then(({ data, errors }) => {
      setExpenses(data.expenses);

      if (errors && errors.find((e) => e.pathError === 'accountResources')) {
        setAccountResources(null);
      } else {
        setAccountResources(data.accountResources);
      }
    }).catch((error) => {
      console.log(error);
    });
  };

  useEffect(() => { fetchData(); },
  // eslint-disable-next-line
  []);

  return (
    <div>
      <button type="button" onClick={() => fetchData()}>
          Atualizar
      </button>
      <aside className="box-shadow">
        <AccountResources accountResources={accountResources} />
        <Simulation />
      </aside>
      <div className="App">
        <h1>Extrato</h1>
        <div className="cards">
          {expenses && expenses.map((e, i) => <Card expense={e} key={i} />)}
        </div>
      </div>
    </div>
  );
};

export default Home;
