FROM node:13.7.0-alpine3.10

WORKDIR /usr/src/app

COPY package*.json ./

RUN npm install

COPY . .

EXPOSE 3000
ENV REACT_APP_API_GRAPHQL_HOST=http://localhost:4005
ENV REACT_APP_DOMAIN=leviditomazzo.auth0.com
ENV REACT_APP_CLIENT_ID=b1oE7t6uLIYpBv0OYSD28VJTCFTvYxcN
ENV REACT_APP_AUDIENCE=https://levi-api-auth0
CMD [ "npm", "start" ]